package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.HashSet;
import java.util.Set;

/**
 * The type Digital wallet.
 */
public class DigitalWallet {

    private Set<DigitalBadge> allBadges;

    private Set<Long> deadLinesPositions;

    private Set<DigitalBadge> deletingBadges = new HashSet<DigitalBadge>();

    /**
     * Instantiates a new Digital wallet.
     *
     * @param allBadges          the all badges
     * @param deadLinesPositions the dead lines positions
     */
    public DigitalWallet(Set<DigitalBadge> allBadges, Set<Long> deadLinesPositions) {
        this.allBadges = allBadges;
        this.deadLinesPositions = deadLinesPositions;
    }

    /**
     * Permet de finaliser la suppression d'un badge au niveau de cet objet
     * pour continuer à reflèter l'état du Wallet à cet instant.
     *
     * @param badge the badge
     */
    public void commitBadgeDeletion(DigitalBadge badge){
        deletingBadges.remove(badge);
        allBadges.remove(badge);
        deadLinesPositions.add(badge.getMetadata().getWalletPosition());
    }


    /**
     * Adds a deleting badges.
     *
     * @param deletingBadge the deleting badge
     */
    public void addDeletingBadge(DigitalBadge deletingBadge) {
        this.deletingBadges.add(deletingBadge);
    }

    /************************************************** GETTERS / SETTERS *******************************************************/


    /**
     * Gets all badges.
     *
     * @return the all badges
     */
    public Set<DigitalBadge> getAllBadges() {
        return allBadges;
    }

    /**
     * Sets all badges.
     *
     * @param allBadges the all badges
     */
    public void setAllBadges(Set<DigitalBadge> allBadges) {
        this.allBadges = allBadges;
    }

    /**
     * Gets dead lines positions.
     *
     * @return the dead lines positions
     */
    public Set<Long> getDeadLinesPositions() {
        return deadLinesPositions;
    }

    /**
     * Sets dead lines positions.
     *
     * @param deadLinesPositions the dead lines positions
     */
    public void setDeadLinesPositions(Set<Long> deadLinesPositions) {
        this.deadLinesPositions = deadLinesPositions;
    }

    /**
     * Gets deleting badges.
     *
     * @return the deleting badges
     */
    public Set<DigitalBadge> getDeletingBadges() {
        return deletingBadges;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalWallet{" +
                "allBadges=" + allBadges +
                ", deadLinesPositions=" + deadLinesPositions +
                ", deletingBadges=" + deletingBadges +
                '}';
    }
}
